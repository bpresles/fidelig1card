# FIDELIG1

## Présentation
Ce code est un générateur de carte de fidélité FIDELIJUNE

Il lance la fabrication de six "G1 Portefeuilles" vides à remplir soi-même !
Les cartes FIDELIJUNE sont assemblées dans un fichier PDF pour les imprimer facilement sur une imprimante A4

Son détenteur peut alors utiliser l'identifiant/mot de passe ou le QRCode de gauche pour accéder au portefeuille correspondant.


## Utilisation
Pour une utilisation en ligne de commande, adaptez ces quelques lignes

```
NomFichier="nom_unique_du_pdf" # Correspond au nom du fichier créé dans /tmp/fidelig1/
Style="nom_du_theme"
Nom="nom_en_haut_a_gauche"
./FIDELIG1.sh "$NomFichier" "$Style" "$Nom"
```

Pour personnaliser vos cartes FideliG1, utilisez le formulaire présenté lors de l'accès au fichier index.php servi par un serveur web qui gère PHP...

## Installation

Pour Linux le seul système d'opération qui respecte votre liberté

```
# Installer git
sudo apt install git
```


```
# Cloner le code de FideliG1Card
git clone https://git.duniter.org/bpresles/fidelig1card.git
cd fidelig1card

```

Ajoutez les dépendances nécessaire 

```
sudo apt install python3 python3-pip imagemagick qrencode poppler-utils
sudo pip3 install duniterpy
```

NB: Si une erreur du type "not autorized" apparait, vous devez autoriser la création de pdf en editant /etc/ImageMagick-6/policy.xml pour commenter la ligne:
```
<!-- <policy domain="coder" rights="none" pattern="PDF" /> -->
```

## Support

Vous avez des questions? Contactez [Bertrand](mailto:bertrand@presles.fr)
