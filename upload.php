<?php

    const IMAGE_JPG = 'image/jpg';
    const IMAGE_JPEG = 'image/jpeg';
    const IMAGE_PNG = 'image/png';

    $lang = !empty($_GET['lang']) ? $_GET['lang'] : 'fr';
    $strings = json_decode(file_get_contents(dirname(__FILE__) . '/translations/' . $lang . '/strings.json' ));
    $target_dir = "images/";

    $pattern_validation_phone = '%[+]?[(]?[)]?[0-9]{1,14}%';
    $pattern_validation_email = '%(?:[a-z0-9!#$\%\&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$\%\&\'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])%';

    function convertAndResize($srcImg, $width, $height, $orig_mime_type='image/png', $type='PNG') {

        // Image properties
        list($width_orig, $height_orig) = getimagesize($srcImg);

        switch ($orig_mime_type) {
            case IMAGE_JPG:
            case IMAGE_JPEG:
                $image = imagecreatefromjpeg($srcImg); 
                break;   
            case IMAGE_PNG:  
                $image = imagecreatefrompng($srcImg);
                break; 
            default:
                throw new Exception('Unrecognized image type ' . $orig_mime_type);
        }

        // create a new blank image
        $newImage = imagecreatetruecolor($width, $height);

        if ($type === 'PNG') {
            imagealphablending($newImage, true);

            $transparent = imagecolorallocatealpha($newImage, 0, 0, 0, 127);
            imagefill($newImage, 0, 0, $transparent);
        }

        // Copy the old image to the new image
        imagecopyresampled($newImage, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

        // Output to a temp file
        $destFile = tempnam('/tmp/fidelig1', 'temp_');

        switch ($type) {
            case 'JPG':
                imagejpeg($newImage, $destFile);
                break;

            default:
                imagealphablending($newImage, false);
                imagesavealpha($newImage, true);

                imagepng($newImage, $destFile); 
                break;
        } 

        // Free memory                           
        imagedestroy($newImage);

        return $destFile;
    }

    // Check if image file is an actual image or a fake image
    if(isset($_POST["submit"])) {
        $style = $_POST['style'];
        $bg_file = $target_dir . 'fond' . $style . '.jpg';
        $logo1_file = $target_dir . 'g1' . $style . '.png';
        $logo2_file = $target_dir . 'logo' . $style . '.png';

        $validationOk = 1;
        if (empty($_POST['style']) && 
            (!empty($_FILES["bg_image"]["tmp_name"])
            || !empty($_FILES["logo1"]["tmp_name"])
            || !empty($_FILES["logo2"]["tmp_name"]))) {
            echo $strings->validation_style . "<br/>";
            $validationOk = 0;
        }

        if (!empty($_POST['company_phone']) && !preg_match($pattern_validation_phone, $_POST['company_phone'])) {
            echo $strings->validation_phone . "<br/>";
            $validationOk = 0;
        }

        if (!empty($_POST['company_email']) && !preg_match($pattern_validation_email, $_POST['company_email'])) {
            echo $strings->validation_email . "<br/>";
            $validationOk = 0;
        }

        // Check if style already exists
        $existing_style = false;
        if (empty($_POST['style']) || (!empty($_POST['style']) && (file_exists($bg_file) || file_exists($logo1_file) || file_exists($logo2_file)))) {
            $existing_style = true;
        }

        if (!$existing_style && !empty($_POST['style']) && !preg_match('%^[a-z0-9]+$%', $_POST['style'])) {
            echo $strings->validation_style_format . "<br/>";
            $validationOk = 0;
        }

        if (!$existing_style && file_exists($_FILES["bg_image"]["tmp_name"])) {
            $bg_check = getimagesize($_FILES["bg_image"]["tmp_name"]);
            $bg_type = mime_content_type($_FILES["bg_image"]["tmp_name"]);
            
            if($bg_check !== false && $bg_type != IMAGE_PNG && $bg_type != IMAGE_JPG && $bg_type != IMAGE_JPEG) {
                echo $strings->validation_format_bg . "<br/>";
                $validationOk = 0;
            }

            if ($validationOk == 1) {
                $bg_path = convertAndResize($_FILES["bg_image"]["tmp_name"], 1013, 638, $bg_type, 'JPG');
            }
        }


        if (!$existing_style && file_exists($_FILES["logo1"]["tmp_name"])) {
            $logo1_check = getimagesize($_FILES["logo1"]["tmp_name"]);
            $logo1_type = mime_content_type($_FILES["logo1"]["tmp_name"]);

            if($logo1_check !== false && $logo1_type != IMAGE_PNG) {
                echo $strings->validation_format_others . "<br/>";
                $validationOk = 0;
            }

            if ($validationOk == 1) {
                $logo1_path = convertAndResize($_FILES["logo1"]["tmp_name"], 180, 180, $logo1_type, 'PNG');
            }
        }


        if (!$existing_style && file_exists($_FILES["logo2"]["tmp_name"])) {
            $logo2_check = getimagesize($_FILES["logo2"]["tmp_name"]);
            $logo2_type = mime_content_type($_FILES["logo2"]["tmp_name"]);

            if($logo2_check !== false && $logo2_type != IMAGE_PNG) {
                echo $strings->validation_format_others . "<br/>";
                $validationOk = 0;
            }

            if ($validationOk == 1) {
                $logo2_path = convertAndResize($_FILES["logo2"]["tmp_name"], 200, 155, $logo2_type, 'PNG');
            }
        }

        if($validationOk === 1) {
            if (!$existing_style && file_exists($_FILES["bg_image"]["tmp_name"]) && !rename($bg_path, $bg_file)) {
                echo sprintf($strings->file_upload_error, $bg_file);
                @unlink($bg_file);
                @unlink($logo1_file);
                @unlink($logo2_file);
                exit();
            }

            if (!$existing_style && file_exists($_FILES["logo1"]["tmp_name"]) && !rename($logo1_path, $logo1_file)) {
                echo sprintf($strings->file_upload_error, $logo1_file);
                @unlink($bg_file);
                @unlink($logo1_file);
                @unlink($logo2_file);
                exit();
            }

            if (!$existing_style && file_exists($_FILES["logo2"]["tmp_name"]) && !rename($logo2_path, $logo2_file)) {
                echo sprintf($strings->file_upload_error, $logo2_file);
                @unlink($bg_file);
                @unlink($logo1_file);
                @unlink($logo2_file);
                exit();
            }

            $dataForGeneration = [
                'style' => $style,
                'name' => $_POST['company'],
                'company_name' => $_POST['company_name'],
                'company_address1' => $_POST['company_address1'],
                'company_address2' => $_POST['company_address2'],
                'company_phone' => $_POST['company_phone'],
                'company_email' => $_POST['company_email'],
                'security' => $_POST['security'],
                'password' => !empty($_POST['passcode']) ? $_POST['passcode'] : '',
                'bottom_message' => (!empty($_POST['product_name']) ? $_POST['product_name'] : $strings->label_fidelity_cards),
                'instructions_card' => $_POST['instructions_card'],
                'security_instructions' => $_POST['security_instructions']
            ];

            $mytime = new Datetime("now");
            $timestamp = $mytime->format('U').rand();
            mkdir(__DIR__ . '/generated/' . $timestamp, 0777, true);
            file_put_contents(__DIR__ . '/generated/' . $timestamp . '/data.json', json_encode($dataForGeneration));

            header("refresh: 1; url=generate.php?uniqid=" . $timestamp . '&lang=' . $lang);
        }
    }
?>

<?php if ($validationOk === 1): ?>
<!DOCTYPE html>
<html>
    <head>
    <title><?php echo $strings->confirmation; ?></title>
    <meta name="robots" content="noindex, nofollow">
    <link rel="stylesheet" type="text/css" href="css/main.css" media="screen"></style>
    </head>
    <body>
        <div class="content">
            <p><?php echo $strings->cards_generation; ?></p>
        </div>
    </body>
</html>
<?php endif; ?>