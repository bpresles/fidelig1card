#!/bin/bash

UNIQID="$1"
PRIVKEY="$2"

mkdir -p "/tmp/fidelig1/qrcodes"
qrencode -s 6 -o "/tmp/fidelig1/qrcodes/${UNIQID}-private.QR.png" "$PRIVKEY"

if ls /tmp/fidelig1/qrcodes/${UNIQID}-private.QR.png 1> /dev/null 2>&1
then
	convert -density 150 /tmp/fidelig1/qrcodes/${UNIQID}-private.QR.png /tmp/fidelig1/qrcodes/${UNIQID}.qrcode.pdf
    rm /tmp/fidelig1/qrcodes/${UNIQID}-private.QR.png
fi