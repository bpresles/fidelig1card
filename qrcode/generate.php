<?php
$mytime = new Datetime("now");
$timestamp = $mytime->format('U').rand();

$privkey_shell = escapeshellcmd('python3 '.dirname(__FILE__).'/../get_privkey_password.py "'.$_POST['identifier'].'" "'.$_POST['password'].'" "'.$_POST['passphrase'].'"');
$privkey = shell_exec($privkey_shell);

$exec_string = escapeshellcmd(dirname(__FILE__)."/qrcode_generator.sh '".$timestamp."' '".trim($privkey)."'");
shell_exec($exec_string);

$attachment_location = '/tmp/fidelig1/qrcodes/'.$timestamp.'.qrcode.pdf';
if (file_exists($attachment_location)) {
    header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
    header("Cache-Control: public"); // needed for internet explorer
    header("Content-Type: application/pdf");
    header("Content-Transfer-Encoding: Binary");
    header("Content-Length:".filesize($attachment_location));
    header("Content-Disposition: attachment; filename=".$timestamp.".qrcode.pdf");
    readfile($attachment_location);

    unlink($attachment_location);
}

die();
?>