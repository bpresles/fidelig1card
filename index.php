<?php
    $lang = !empty($_GET['lang']) ? $_GET['lang'] : 'fr';
    $strings = json_decode(file_get_contents(dirname(__FILE__) . '/translations/' . $lang . '/strings.json' ));

    $images = scandir(dirname(__FILE__).'/images');
    $styles = [];
    foreach ($images as $image) {
        if (preg_match('%^fond([^\.]+).*$%', $image, $matches) && $matches[1] !== '_blank') {
            $styles[] = $matches[1];
        }
    }
?>
<!DOCTYPE html>

<html lang="fr">
    
    <head>
        <meta charset=utf-8>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="title" value="<?php echo $strings->meta_title; ?>"></meta>
    	<meta name="description" value="<?php echo $strings->meta_description; ?>"></meta>
        <title><?php echo $strings->title; ?></title>
        <!-- Load Roboto font -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <!-- Load css styles -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/pluton.css" />
        <!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="css/pluton-ie7.css" />
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="css/jquery.cslider.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
        <link rel="stylesheet" type="text/css" href="css/animate.css" />
		<link rel="stylesheet" type="text/css" href="css/main.css" media="screen"></style>
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57.png">
        <link rel="shortcut icon" href="images/ico/favicon.ico">
    </head>
    
    <body>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <a href="#" class="brand">
                        <img src="images/<?php echo $lang; ?>/logo-g1cards.png" width="120" height="40" alt="Logo" />
                        <!-- This is website logo -->
                    </a>
                    <!-- Navigation button, visible on small resolution -->
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <i class="icon-menu"></i>
                    </button>
                    <!-- Main navigation -->
                    <div class="nav-collapse collapse pull-right">
                        <ul class="nav" id="top-navigation">
                            <li class="active"><a href="#home"><?php echo $strings->label_menu_home; ?></a></li>
                            <li><a href="#generateur"><?php echo $strings->label_menu_generator; ?></a></li>
                            <li><a href="#portfolio"><?php echo $strings->label_menu_porfolio; ?></a></li>
						  <li><a href="#about"><?php echo $strings->label_menu_about; ?></a></li>
                            <li><a href="#contact"><?php echo $strings->label_menu_contact; ?></a></li>
                        </ul>
                    </div>
                    <!-- End main navigation -->
                </div>
            </div>
        </div>
        <!-- Start home section -->
        <div id="home">
            <!-- Start cSlider -->
            <div id="da-slider" class="da-slider">
                <div class="triangle"></div>
                <!-- mask elemet use for masking background image -->
                <div class="mask"></div>
                <!-- All slides centred in container element -->
                <div class="container">
                    <!-- Start first slide -->
                    <div class="da-slide">
                      <div class="slide-left">
                        <h2 class="fittext2"><?php echo $strings->label_welcome_g1cards; ?></h2>
                        <h4><?php echo $strings->label_welcome_description; ?></h4>
                        <p><?php echo $strings->label_welcome_description2; ?></p>
                      </div>
                      <div class="slide-right">
                        <div class="da-img">
                            <img src="images/Slider01.png" alt="image01" width="320">
                        </div>
                      </div>
                    </div>
                    <!-- End first slide -->
                    <!-- Start second slide -->
                    <div class="da-slide">
                      <div class="slide-left">
                        <h2><?php echo $strings->label_details_g1cards; ?></h2>
                        <h4><?php echo $strings->label_details_description; ?></h4>
                        <p><?php echo $strings->label_details_description2; ?></p>
                        <a href="#generateur" class="da-link button"><?php echo $strings->label_try_g1card_generator; ?></a>
                      </div>
                      <div class="slide-right">
                        <div class="da-img">
                          <img src="images/Slider02.png" width="320" alt="image02">
                        </div>
                      </div>
                    </div>
                    <!-- End second slide -->
                    <!-- Start third slide -->
                    <div class="da-slide">
                      <div class="slide-left">
                        <h2><?php echo $strings->label_app_g1cards; ?></h2>
                        <h4><?php echo $strings->label_app_description; ?></h4>
                        <p><?php echo $strings->label_app_description2; ?></p>
                        <a href="#hiring" class="da-link button">Nous recrutons !</a>
                      </div>
                      <div class="slide-right">
                        <div class="da-img">
                          <img src="images/Slider03.png" width="320" alt="image03">
                        </div>
                      </div>
                    </div>
                    <!-- Start third slide -->
                    <!-- Start cSlide navigation arrows -->
                    <div class="da-arrows">
                        <span class="da-arrows-prev"></span>
                        <span class="da-arrows-next"></span>
                    </div>
                    <!-- End cSlide navigation arrows -->
                </div>
            </div>
        </div>
        <!-- End home section -->
        <!-- Service section start -->
        <div class="section primary-section" id="generateur">
            <div class="container">
                <!-- Start title section -->
                <div class="content">
            <h1><?php echo $strings->body_head_title; ?></h1>
            <p class="bold"><?php echo $strings->body_description; ?></p>
            <form action="upload.php?lang=<?php echo !empty($_GET['lang']) ? $_GET['lang'] : 'fr'; ?>" method="POST" enctype="multipart/form-data">
                <label for="language"><?php echo $strings->form_language; ?></label><br/>
                <select name="language" id="language">
                    <option value="en"<?php echo (!empty($_GET['lang']) && $_GET['lang'] === 'en' ? ' selected="selected"' : ''); ?>>EN</option>
                    <option value="fr"<?php echo (empty($_GET['lang']) || $_GET['lang'] === 'fr' ? ' selected="selected"' : ''); ?>>FR</option>
                </select><br/><br/>
                <label for="style_select"><?php echo $strings->form_styles; ?></label><br/>
                <input type="radio" name="style_select" value="" checked="checked"> <?php echo $strings->form_style_new; ?><br/>
                <?php foreach ($styles as $style): ?>
                <input type="radio" name="style_select" value="<?php echo $style; ?>"> <?php echo $style; ?><br/>
                <?php endforeach; ?><br/>
                <label for="style"><?php echo $strings->form_style_name; ?><br/>
                <span>(<?php echo $strings->validation_style_format; ?>)</span></label><br/>
                <input type="text" name="style" id="style"><br/><br/>
                <label for="company"><?php echo $strings->form_company; ?></label><br/>
                <input type="text" name="company" id="company"><br/><br/>
                <label for="company"><?php echo $strings->form_product_name; ?></label><br/>
                <input type="text" name="product_name" id="product_name" value="<?php echo $strings->label_fidelity_cards ?>"><br/><br/>
                <label for="security"><?php echo $strings->form_security; ?></label>
                <p class="bold"><?php echo $strings->body_security_card; ?></p>
                <input type="radio" name="security" value="qrpassword" checked="checked"><?php echo $strings->form_security_qrpassword; ?><br/>
                <input type="radio" name="security" value="qridspassword"><?php echo $strings->form_security_qrids_password; ?><br/>
                <input type="hidden" name="security_instructions" value="y">
                <input type="hidden" name="instructions_card" value="y">
                <br/><br/>
                <p class="bold"><?php echo $strings->body_company_details; ?></p>
                <label for="security"><?php echo $strings->form_company_name; ?></label><br/>
                <input type="text" name="company_name" id="company_name"><br/><br/>
                <label for="security"><?php echo $strings->form_company_address; ?></label><br/>
                <input type="text" name="company_address1" id="company_address1"><br/><br/>
                <input type="text" name="company_address2" id="company_address2"><br/><br/>
                <label for="security"><?php echo $strings->form_company_phone; ?></label><br/>
                <input type="text" name="company_phone" id="company_phone"><br/><br/>
                <label for="security"><?php echo $strings->form_company_email; ?></label><br/>
                <input type="text" name="company_email" id="company_email"><br/><br/>
                <p class="bold"><?php echo $strings->body_personalize; ?></p>
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <label for="bg_image"><?php echo $strings->form_background_image; ?></label><br/>
                                <input type="file" name="bg_image" id="bg_image">
                            </td>
                            <td style="padding-left: 20px;">
                                <?php echo $strings->img_sample; ?><br/><img src="images/fond.jpg" alt="Background" style="width: 337px;" id="bgimg">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="logo1"><?php echo $strings->form_picture_logo; ?></label><br/>
                                <input type="file" name="logo1" id="logo1">
                            </td>
                            <td style="padding-left: 20px;">
                                <?php echo $strings->img_sample; ?><br/><img src="images/g1.png" alt="Logo1" style="width: 90px;" id="logo1img">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="logo2"><?php echo $strings->form_picture_bottom_image; ?></label><br/>
                                <input type="file" name="logo2" id="logo2">
                            </td>
                            <td style="padding-left: 20px;">
                                <?php echo $strings->img_sample; ?><br/><img src="images/logo.png" alt="Logo2" style="width: 100px;" id="logo2img">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br/><br/>
                <input type="submit" value="<?php echo $strings->form_submit; ?>" name="submit">
            </form>
        </div>
                <div class="row-fluid">
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                                <img class="img-circle" src="images/Service1.png" alt="service 1">
                            </div>
                            <h3><?php echo $strings->label_marketing_service_1_title; ?></h3>
                            <p><?php echo $strings->label_marketing_service_1_description; ?></p>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                                <img class="img-circle" src="images/Service2.png" alt="service 2" />
                            </div>
                            <h3><?php echo $strings->label_marketing_service_2_title; ?></h3>
                            <p><?php echo $strings->label_marketing_service_2_description; ?></p>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                                <img class="img-circle" src="images/Service3.png" alt="service 3">
                            </div>
                            <h3><?php echo $strings->label_marketing_service_3_title; ?></h3>
                            <p><?php echo $strings->label_marketing_service_3_description; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Service section end -->
        <!-- Portfolio section start -->
        <div class="section secondary-section " id="portfolio">
            <div class="triangle"></div>
            <div class="container">
                <div class=" title">
                    <h1><?php echo $strings->label_porfolio_title; ?></h1>
                    <p><?php echo $strings->label_porfolio_description; ?></p>
                </div>
                <ul class="nav nav-pills">
                    <li class="filter" data-filter="all">
                        <a href="#noAction"><?php echo $strings->label_porfolio_category_all; ?></a>
                    </li>
                    <li class="filter" data-filter="artisan">
                        <a href="#noAction"><?php echo $strings->label_porfolio_category_1; ?></a>
                    </li>
                    <li class="filter" data-filter="restauration">
                        <a href="#noAction"><?php echo $strings->label_porfolio_category_2; ?></a>
                    </li>
                    <li class="filter" data-filter="identite">
                        <a href="#noAction"><?php echo $strings->label_porfolio_category_3; ?></a>
                    </li>
                </ul>
                <!-- Start details for portfolio project 1 -->
                <div id="single-project">
                    <div id="slidingDiv" class="toggleDiv row-fluid single-project">
                        <div class="span6">
                            <img src="images/portfolio-anuastyle.jpeg" alt="project 1" />
                        </div>
                        <div class="span6">
                            <div class="project-description">
                                <div class="project-title clearfix">
                                    <h3>AnuaAnua</h3>
                                    <span class="show_hide close">
                                        <i class="icon-cancel"></i>
                                    </span>
                                </div>
                                <div class="project-info">
                                    
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- End details for portfolio project 1 -->
                    <!-- Start details for portfolio project 2 -->
                    <div id="slidingDiv1" class="toggleDiv row-fluid single-project">
                        <div class="span6">
                            <img src="images/portfolio-cordonneriecaillaud.jpeg" alt="project 2">
                        </div>
                        <div class="span6">
                            <div class="project-description">
                                <div class="project-title clearfix">
                                    <h3>Cordonnerie Caillaud</h3>
                                    <span class="show_hide close">
                                        <i class="icon-cancel"></i>
                                    </span>
                                </div>
                                <div class="project-info">
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- End details for portfolio project 2 -->
                    <!-- Start details for portfolio project 3 -->
                    <div id="slidingDiv2" class="toggleDiv row-fluid single-project">
                        <div class="span6">
                            <img src="images/portfolio-filokayu.jpeg" alt="project 3">
                        </div>
                        <div class="span6">
                            <div class="project-description">
                                <div class="project-title clearfix">
                                    <h3>Filokayu</h3>
                                    <span class="show_hide close">
                                        <i class="icon-cancel"></i>
                                    </span>
                                </div>
                                <div class="project-info">
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- End details for portfolio project 3 -->
                    <!-- Start details for portfolio project 4 -->
                    <div id="slidingDiv3" class="toggleDiv row-fluid single-project">
                        <div class="span6">
                            <img src="images/portfolio-g1sportsante.jpeg" alt="project 4">
                        </div>
                        <div class="span6">
                            <div class="project-description">
                                <div class="project-title clearfix">
                                    <h3>Ğ1 Sport-Santé</h3>
                                    <span class="show_hide close">
                                        <i class="icon-cancel"></i>
                                    </span>
                                </div>
                                <div class="project-info">
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- End details for portfolio project 4 -->
                    <!-- Start details for portfolio project 5 -->
                    <div id="slidingDiv4" class="toggleDiv row-fluid single-project">
                        <div class="span6">
                            <img src="images/portfolio-lebellactfidelig1.jpeg" alt="project 5">
                        </div>
                        <div class="span6">
                            <div class="project-description">
                                <div class="project-title clearfix">
                                    <h3>Le bel act</h3>
                                    <span class="show_hide close">
                                        <i class="icon-cancel"></i>
                                    </span>
                                </div>
                                <div class="project-info">
                                   
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- End details for portfolio project 5 -->
                    <!-- Start details for portfolio project 6 -->
                    <div id="slidingDiv5" class="toggleDiv row-fluid single-project">
                        <div class="span6">
                            <img src="images/portfolio-lebol.jpeg" alt="project 6">
                        </div>
                        <div class="span6">
                            <div class="project-description">
                                <div class="project-title clearfix">
                                    <h3>Le Bol</h3>
                                    <span class="show_hide close">
                                        <i class="icon-cancel"></i>
                                    </span>
                                </div>
                                <div class="project-info">
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- End details for portfolio project 6 -->
                    <!-- Start details for portfolio project 7 -->
                    <div id="slidingDiv6" class="toggleDiv row-fluid single-project">
                        <div class="span6">
                            <img src="images/portfolio-libremarcat83.jpeg" alt="project 7">
                        </div>
                        <div class="span6">
                            <div class="project-description">
                                <div class="project-title clearfix">
                                    <h3>Libre Marcat 83</h3>
                                    <span class="show_hide close">
                                        <i class="icon-cancel"></i>
                                    </span>
                                </div>
                                <div class="project-info">
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- End details for portfolio project 7 -->
                    <!-- Start details for portfolio project 8 -->
                    <div id="slidingDiv7" class="toggleDiv row-fluid single-project">
                        <div class="span6">
                            <img src="images/portfolio-nouvellecaledonie.jpeg" alt="project 8">
                        </div>
                        <div class="span6">
                            <div class="project-description">
                                <div class="project-title clearfix">
                                    <h3>Nouvelle Calédonie</h3>
                                    <span class="show_hide close">
                                        <i class="icon-cancel"></i>
                                    </span>
                                </div>
                                <div class="project-info">
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- End details for portfolio project 8 -->
                    <!-- Start details for portfolio project 9 -->
                    <div id="slidingDiv8" class="toggleDiv row-fluid single-project">
                        <div class="span6">
                            <img src="images/portfolio-tahiti.jpeg" alt="project 9">
                        </div>
                        <div class="span6">
                            <div class="project-description">
                                <div class="project-title clearfix">
                                    <h3>Tahiti</h3>
                                    <span class="show_hide close">
                                        <i class="icon-cancel"></i>
                                    </span>
                                </div>
                                <div class="project-info">
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- End details for portfolio project 9 -->
                    <ul id="portfolio-grid" class="thumbnails row">
                        <li class="span4 mix artisan">
                            <div class="thumbnail">
                                <img src="images/portfolio-anuastyle.jpeg" alt="project 1">
                                <a href="#single-project" class="more show_hide" rel="#slidingDiv">
                                    <i class="icon-plus"></i>
                                </a>
                                <h3>AnuaAnua</h3>
                                <p>Gravure sur bois</p>
                                <div class="mask"></div>
                            </div>
                        </li>
                        <li class="span4 mix artisan">
                            <div class="thumbnail">
                                <img src="images/portfolio-cordonneriecaillaud.jpeg" alt="project 2">
                                <a href="#single-project" class="show_hide more" rel="#slidingDiv1">
                                    <i class="icon-plus"></i>
                                </a>
                                <h3>Cordonnerie Caillaud</h3>
                                <p>Cordonnier artisan</p>
                                <div class="mask"></div>
                            </div>
                        </li>
                        <li class="span4 mix identite">
                            <div class="thumbnail">
                                <img src="images/portfolio-filokayu.jpeg" alt="project 3">
                                <a href="#single-project" class="more show_hide" rel="#slidingDiv2">
                                    <i class="icon-plus"></i>
                                </a>
                                <h3>Filokayu</h3>
                                <p>Promotion Nouvelle Calédonie</p>
                                <div class="mask"></div>
                            </div>
                        </li>
                        <li class="span4 mix identite">
                            <div class="thumbnail">
                                <img src="images/portfolio-g1sportsante.jpeg" alt="project 4">
                                <a href="#single-project" class="show_hide more" rel="#slidingDiv3">
                                    <i class="icon-plus"></i>
                                </a>
                                <h3>Ğ1 Sport-Santé</h3>
                                <p>Projet Maison Sport-Santé - APAP</p>
                                <div class="mask"></div>
                            </div>
                        </li>
                        <li class="span4 mix restauration">
                            <div class="thumbnail">
                                <img src="images/portfolio-lebellactfidelig1.jpeg" alt="project 5">
                                <a href="#single-project" class="show_hide more" rel="#slidingDiv4">
                                    <i class="icon-plus"></i>
                                </a>
                                <h3>Le Bel Act</h3>
                                <p>Bar à jeux</p>
                                <div class="mask"></div>
                            </div>
                        </li>
                        <li class="span4 mix restauration">
                            <div class="thumbnail">
                                <img src="images/portfolio-lebol.jpeg" alt="project 6">
                                <a href="#single-project" class="show_hide more" rel="#slidingDiv5">
                                    <i class="icon-plus"></i>
                                </a>
                                <h3>Le Bol</h3>
                                <p>Restaurant Asiatique</p>
                                <div class="mask"></div>
                            </div>
                        </li>
                        <li class="span4 mix identite">
                            <div class="thumbnail">
                                <img src="images/portfolio-libremarcat83.jpeg" alt="project 7" />
                                <a href="#single-project" class="show_hide more" rel="#slidingDiv6">
                                    <i class="icon-plus"></i>
                                </a>
                                <h3>Libre Marcat 83</h3>
                                <p>Ğmarché Var</p>
                                <div class="mask"></div>
                            </div>
                        </li>
                        <li class="span4 mix identite">
                            <div class="thumbnail">
                                <img src="images/portfolio-nouvellecaledonie.jpeg" alt="project 8">
                                <a href="#single-project" class="show_hide more" rel="#slidingDiv7">
                                    <i class="icon-plus"></i>
                                </a>
                                <h3>Nouvelle Calédonie</h3>
                                <p>Promotion de la Nouvelle Calédonie</p>
                                <div class="mask"></div>
                            </div>
                        </li>
                        <li class="span4 mix identite">
                            <div class="thumbnail">
                                <img src="images/portfolio-tahiti.jpeg" alt="project 9">
                                <a href="#single-project" class="show_hide more" rel="#slidingDiv8">
                                    <i class="icon-plus"></i>
                                </a>
                                <h3>Tahiti</h3>
                                <p>Promotion de Tahiti</p>
                                <div class="mask"></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Portfolio section end -->
        <!-- About us section start -->
        <div class="section primary-section" id="about">
            <div class="triangle"></div>
            <div class="container">
                <div class="title">
                    <h2><?php echo $strings->label_about_us_title; ?></h2>
                    <p><?php echo $strings->label_about_us_description; ?></p>
                </div>
                <div class="row-fluid team">
                    <div class="span4" id="first-person">
                        <div class="thumbnail">
                            <img src="images/BERTRAND-PRESLES.jpeg" alt="Bertrand Presles">
                            <h3><?php echo $strings->label_about_us_contributor1_name; ?></h3>
                            <ul class="social">
                                <li>
                                    <a href="https://www.facebook.com/bertrand.presles">
                                        <span class="icon-facebook-circled"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/bpresles">
                                        <span class="icon-twitter-circled"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/in/bpresles/">
                                        <span class="icon-linkedin-circled"></span>
                                    </a>
                                </li>
                            </ul>
                            <div class="mask">
                                <h2><?php echo $strings->label_about_us_contributor1_job; ?></h2>
                                <p><?php echo $strings->label_about_us_contributor1_mantra; ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="span4" id="second-person">
                        <div class="thumbnail">
                            <img src="images/Francis-Drubigny.png" alt="Francis Drubigny">
                            <h3><?php echo $strings->label_about_us_contributor2_name; ?></h3>
                            <ul class="social">
                                <li>
                                    <a href="https://www.facebook.com/francis.drubigny">
                                        <span class="icon-facebook-circled"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/fdrubigny">
                                        <span class="icon-twitter-circled"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/in/francis-drubigny/">
                                        <span class="icon-linkedin-circled"></span>
                                    </a>
                                </li>
                            </ul>
                            <div class="mask">
                                <h2><?php echo $strings->label_about_us_contributor2_job; ?></h2>
                                <p><?php echo $strings->label_about_us_contributor2_mantra; ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="span4" id="third-person">
                        <div class="thumbnail">
                            <img src="images/Minh-Le-Bol.jpeg" alt="team 1">
                            <h3><?php echo $strings->label_about_us_contributor3_name; ?></h3>
                            <ul class="social">
                                <li>
                                    <a href="https://www.facebook.com/Le-Bol-114949453283074/">
                                        <span class="icon-facebook-circled"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://le-bol-restaurant-saint-herblain.eatbu.com/">
                                        <span class="icon-globe"></span>
                                    </a>
                                </li>
                                
                            </ul>
                            <div class="mask">
                                <h2><?php echo $strings->label_about_us_contributor3_job; ?></h2>
                                <p><?php echo $strings->label_about_us_contributor3_mantra; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="about-text centered" id="hiring">
                    <h3><?php echo $strings->label_about_g1cards; ?></h3>
                    <p><?php echo $strings->label_about_g1cards_description; ?><br><a href="https://forum.duniter.org/t/appel-a-candidatures-pour-le-projet-dapp-fidelig1/8495" target="_blank" ><button class="button button-sp"><?php echo $strings->label_about_g1cards_link; ?></button></a></p>
                </div>
                <h3>Skills</h3>
                <div class="row-fluid">
                    <div class="span6">
                        <ul class="skills">
                            <li>
                                <span class="bar" data-width="80%"></span>
                                <h3>UX / UI Design</h3>
                            </li>
                            <li>
                                <span class="bar" data-width="95%"></span>
                                <h3>JS / Flutter</h3>
                            </li>
                            <li>
                                <span class="bar" data-width="68%"></span>
                                <h3>Marketing / Communication</h3>
                            </li>
                            <li>
                                <span class="bar" data-width="70%"></span>
                                <h3>HTML5 / CSS3</h3>
                            </li>
                        </ul>
                    </div>
                    <div class="span6">
                        <div class="highlighted-box center">
                            <h1><?php echo $strings->label_job_offer_title; ?></h1>
                            <p><?php echo $strings->label_job_offer_description; ?></p>
                            <a href="mailto:fidelig1@cloud-libre.eu"><button class="button button-sp"><?php echo $strings->label_job_offer_apply; ?></button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About us section end -->
        <div class="section secondary-section">
            <div class="triangle"></div>
            <div class="container centered">
                <p class="large-text"><?php echo $strings->label_about_us_mantra1; ?></p>
            </div>
        </div>
        <!-- Client section start -->
        <!-- Client section start -->
        <div id="clients">
            <div class="section primary-section">
                <div class="triangle"></div>
                <div class="container">
                  <p class="testimonial-text">
                      "<?php echo $strings->label_about_us_mantra2; ?>"
                    </p>
                </div>
            </div>
        </div>
        <div class="section third-section">
            <div class="container centered"></div>
        </div>
        <!-- Price section start -->
        <div id="price" class="section secondary-section">
            <div class="container" id="contact">
                <div class="title">
                    <h2><?php echo $strings->label_contribution_title; ?></h2>
                    <p><?php echo $strings->label_contribution_description; ?><br> <br>FxJG7dFnkKJhTUmBQUdLz7FjVDS7NESDPNeSKKhq24CP</p>
                </div>
				<div class="span9 center contact-info">
                        <p class="info-mail">fidelig1 [At] cloud-libre.eu</p>
              </div>
            </div>
        </div>
        <!-- Price section end -->
        <!-- Newsletter section start --><!-- Newsletter section end -->
        <!-- Contact section start --><!-- Contact section edn -->
        <!-- Footer section start -->
        <div class="footer">
            <p>2021 <?php echo $strings->project_name; ?> | <a href="https://git.duniter.org/bpresles/fidelig1card/-/blob/master/LICENSE">Licence AGPL v.3</a> | <?php echo $strings->label_website_credits; ?> <a href="https://f.drubigny.fr">Francis Drubigny</a></p>
        </div>
        <!-- Footer section end -->
        <!-- ScrollUp button start -->
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
        <!-- ScrollUp button end -->
        <!-- Include javascript -->
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
       
        <!-- css3-mediaqueries.js for IE8 or older -->
        <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/app.js"></script>
		<script type="text/javascript" src="js/jquery.min.js"></script>
    	<script type="text/javascript" src="js/main.js"></script>
    </body>
</html>
