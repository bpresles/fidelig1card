<?php
$lang = !empty($_GET['lang']) ? $_GET['lang'] : 'fr';
$strings = json_decode(file_get_contents(dirname(__FILE__) . '/translations/' . $lang . '/strings.json' ));
$timestamp = $_GET['uniqid'];

$data = json_decode(file_get_contents(__DIR__ . '/generated/' . $timestamp . '/data.json'), true);

// CREATE 6 G1BILLETS in /tmp/g1billet/$timestamp
$exec_string = 'FIDELIG1_FIDELITY_CARDS_MSG="'.$data['bottom_message'].'"';
$exec_string .= ' FIDELIG1_SEND_MSG="'.$strings->label_send.'"';
$exec_string .= ' FIDELIG1_RECEIVE_MSG="'.$strings->label_receive.'"';

$exec_string .= ' FIDELIG1_IDENTIFIER_MSG="'.$strings->label_identifier.'"';
$exec_string .= ' FIDELIG1_SECRET_CODE_MSG="'.$strings->label_secret_code.'"';
$exec_string .= ' FIDELIG1_SECRET_MSG="'.$strings->label_secret.'"';

$exec_string .= ' FIDELIG1_USE_YOUR_G1_MSG="'.$strings->label_use_your_g1.'"';
$exec_string .= ' FIDELIG1_DL_CESIUM_MSG="'.$strings->label_download_cesium.'"';
$exec_string .= ' FIDELIG1_CONNECT_MSG="'.$strings->label_connect.'"';

$exec_string .= ' FIDELIG1_TYPE_IDS1_MSG="'.$strings->label_type_ids1.'"';
$exec_string .= ' FIDELIG1_TYPE_IDS2_MSG="'.$strings->label_type_ids2.'"';
$exec_string .= ' FIDELIG1_SCAN_QR1_MSG="'.$strings->label_scan_qr1.'"';
$exec_string .= ' FIDELIG1_SCAN_QR2_MSG="'.$strings->label_scan_qr2.'"';
$exec_string .= ' FIDELIG1_SCAN_QR3_MSG="'.$strings->label_scan_qr3.'"';

$exec_string .= ' FIDELIG1_TEL_MSG="'.$strings->label_tel.'"';
$exec_string .= ' FIDELIG1_EMAIL_MSG="'.$strings->label_email.'"';
$exec_string .= ' FIDELIG1_SECURITY_CONNECT_MSG="'.$strings->label_security_connect.'"';
$exec_string .= ' FIDELIG1_SECURITY_CONNECT_QR_OR_MSG="'.$strings->label_security_connect_qr_or.'"';
$exec_string .= ' FIDELIG1_SECURITY_CONNECT_QR_MSG="'.$strings->label_security_connect_qr.'"';
$exec_string .= ' FIDELIG1_SECURITY_CONNECT_QR_SECRET_MSG="'.$strings->label_security_connect_qr_secret.'"';
$exec_string .= ' FIDELIG1_USEFUL_LINKS_MSG="'.$strings->label_useful_links.'"';
$exec_string .= ' FIDELIG1_LINK_WHATS_G1_MSG="'.$strings->label_link_whats_g1.'"';
$exec_string .= ' FIDELIG1_LINK_FORUM_MSG="'.$strings->label_link_forum.'"';
$exec_string .= ' FIDELIG1_LINK_GCHANGE_MSG="'.$strings->label_link_gchange.'"';
$exec_string .= ' FIDELIG1_LINK_CESIUM_MSG="'.$strings->label_link_cesium.'"';

$exec_string .= ' '.dirname(__FILE__).'/FIDELIG1.sh "'.$timestamp.'" "'.$data['style'].'" "'.$data['name'].'" "'.$data['company_name'].'" "'.$data['company_address1'].'" "'.$data['company_address2'].'" "'.$data['company_phone'].'" "'.$data['company_email'].'" "'.$data['security'].'" "'.$data['password'].'" "'.$data['instructions_card'].'" "'.$data['security_instructions'].'" "'.$lang.'"';

echo shell_exec($exec_string);

if (file_exists(__DIR__ . '/generated/' . $timestamp . '/' . $timestamp . '.cards.pdf')): ?>
<!DOCTYPE html>
<html>
    <head>
    <title><?php echo $strings->confirmation; ?></title>
    <meta name="robots" content="noindex, nofollow">
    <link rel="stylesheet" type="text/css" href="css/main.css" media="screen"></style>
    </head>
    <body>
        <div class="content">
            <h2><?php echo $strings->generation_success; ?></h2>
            <?php if (!empty($data['password'])): ?>
            <p>
            <?php echo $strings->password_reminder; ?>&nbsp;<span style="font-weight: bold;"><?php echo $data['password']; ?></span>
            <br/>
            <?php echo $strings->password_reminder2; ?>
            </p>
            <?php endif; ?>
            <p>
                <a href="generated/<?php echo $timestamp; ?>/<?php echo $timestamp; ?>.cards.pdf" title="<?php echo $strings->cards_pdf; ?>" target="_blank"><?php echo $strings->cards_pdf; ?></a><br/>
            </p>
        </div>
    </body>
</html>
<?php else : ?>
	Error: File not found.
<?php endif; ?>
