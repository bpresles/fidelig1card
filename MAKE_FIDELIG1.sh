#!/bin/bash
################################################################################
# Author: Fred (support@qo-op.com)
# Version: 0.1
# License: AGPL-3.0 (https://choosealicense.com/licenses/agpl-3.0/)
################################################################################
# INSTALLER convert et qrencode: sudo apt install imagemagick qrencode
################################################################################
MY_PATH="`dirname \"$0\"`"              # relative
MY_PATH="`( cd \"$MY_PATH\" && pwd )`"  # absolutized and normalized
ME="${0##*/}"
############################################################################################################################################################
# ${MY_PATH}/MAKEFIDELIG1.sh "nu me ro test" "se cr et" 100 7sn9dKeCNEsHmqm1gMWNREke4YAWtNw8KG1YBSN8CmSh 97968583
############################################################################

NUMERO="$1"
SECRET="$2"
PUBKEY="$3"
PRIVKEY="$4"
UNIQKEY="$5"
UNIQID="$6"
STYLE="$7"
NAME="$8"
COMPANY_NAME="$9"
COMPANY_ADDRESS_1="${10}"
COMPANY_ADDRESS_2="${11}"
COMPANY_PHONE="${12}"
COMPANY_EMAIL="${13}"
SECURITY="${14}"
PRINT_INSTRUCTIONS="${15}"
PRINT_SECURITY="${16}"
LANG="${17}"

BGEXTENSION="jpg"

if [[ "$NUMERO" == "" || "$SECRET" == "" || "$PUBKEY" == "" || "$UNIQID" == "" || "$SECURITY" == "" ]]
then
	exit 1
fi

if [[ "$NAME" == "" ]]
then
	NAME="FideliĞ1"
fi


mkdir -p /tmp/fidelig1/$UNIQID
CARDNAME=$(echo $NUMERO | sed 's/ /_/g')

# Front of the cards generation.

# Prepare CARD qrcode verification URL
qrencode -s 6 -o "/tmp/fidelig1/${UNIQID}/${CARDNAME}.QR.png" "$PUBKEY"
if [[ "$PRIVKEY" != "" ]]
then
	qrencode -s 6 -o "/tmp/fidelig1/${UNIQID}/${CARDNAME}-private.QR.png" "$PRIVKEY"
fi

# Copy bg image
if [[ -f ${MY_PATH}/images/fond${STYLE}.${BGEXTENSION} ]]
then
	cp "${MY_PATH}/images/fond${STYLE}.${BGEXTENSION}" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}"
else
	BGEXTENSION="png"
	cp "${MY_PATH}/images/fond_blank.${BGEXTENSION}" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}"
fi

# Add text zones
convert -fill "rgba(255,255,255,0.8)" -draw 'rectangle 0,0 1013,145' \
-fill white -draw 'rectangle 757,252 978,280' \
-fill "rgba(255,255,255,0.8)" -draw 'rectangle 0,585 1013,638' \
"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}"

# Add private key QRCode
if [ "$PRIVKEY" != "" ] && [ "$UNIQKEY" != "" ] && [ "$PRINT_SECURITY" = "n" ]
then
	convert -fill white -draw 'rectangle 34,150 256,210' \
	"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}"

	composite -compose Over -gravity SouthWest -geometry +35+230 "/tmp/fidelig1/${UNIQID}/${CARDNAME}-private.QR.png" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}"
else 
	if [[ "$PRIVKEY" != "" ]]
	then
		convert -fill white -draw 'rectangle 34,155 256,210' \
		"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}"

		composite -compose Over -gravity SouthWest -geometry +35+230 "/tmp/fidelig1/${UNIQID}/${CARDNAME}-private.QR.png" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}"
	fi
fi

# Add borders
convert -bordercolor black -border 1 \
"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}"

# Add verification QRCode
composite -compose Over -gravity SouthEast -geometry +35+140 "/tmp/fidelig1/${UNIQID}/${CARDNAME}.QR.png" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}"

# Copy G1 Logo
if [[ -f ${MY_PATH}/images/g1${STYLE}.png ]]
then
	# Add June LOGO to card
	composite -compose Over -gravity NorthEast -geometry +50+50  -dissolve 90% -colorspace sRGB "${MY_PATH}/images/g1${STYLE}.png" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}"
fi

if [[ -f ${MY_PATH}/images/logo${STYLE}.png ]]
then
	# Add ${MY_PATH}/images/logo${STYLE}.png (250px)
	composite -compose Over -gravity SouthWest -geometry +45+65 -dissolve 70%  -colorspace sRGB "${MY_PATH}/images/logo${STYLE}.png" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}"
fi

XZUID="${NAME}"
BOTTOM="${FIDELIG1_FIDELITY_CARDS_MSG}          https://www.monnaie-libre.fr         https://cesium.app"

# Do not add authentication informations when using a password on private QRCode. In that case only QRCode authentication with password is allowed.
if ([ "$SECURITY" = "qrids" ] || [ "$SECURITY" = "ids" ] || [ "$SECURITY" = "qridspassword" ]) && [ "$PRINT_SECURITY" = "n" ]
then
	convert -font 'LiberationSans' -weight 900 \
	-pointsize 50 -fill black -draw 'text 50,65 "'"$XZUID"'"' \
	-pointsize 30 -fill black -draw 'text 50,100 "'"$FIDELIG1_IDENTIFIER_MSG"'"' \
	-pointsize 30 -fill black -draw 'text 50,130 "'"$FIDELIG1_SECRET_CODE_MSG"'"' \
	"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}"

	convert -font 'LiberationSans' \
	-pointsize 30 -fill black -draw 'text 210,100 "'"$NUMERO"'"' \
	-pointsize 30 -fill black -draw 'text 240,130 "'"$SECRET"'"' \
	"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}"

else
	convert -font 'LiberationSans' -weight 900 \
	-pointsize 50 -fill black -draw 'text 50,90 "'"$XZUID"'"' \
	"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}"
fi

convert -font 'LiberationSans' \
-pointsize 20 -fill black -draw 'text 450,20 "'"${PUBKEY}"'"' \
-pointsize 30 -fill black -draw 'text 810,285 "'"${FIDELIG1_RECEIVE_MSG}"'"' \
-pointsize 25 -fill black -draw 'text 50,620 "'"$BOTTOM"'"' \
"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}"

if [ "$PRIVKEY" != "" ] && [ "$UNIQKEY" != "" ] && [ "$PRINT_SECURITY" = "n" ]
then
	convert -font 'LiberationSans' \
	-pointsize 30 -fill black -draw 'text 90,180 "'"${FIDELIG1_SEND_MSG}"'"' \
	-pointsize 20 -fill black -draw 'text 65,200 "('"${FIDELIG1_SECRET_MSG}"' @'"${UNIQKEY}"')"' \
	"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}"
else 
	if [[ "$PRIVKEY" != "" ]]
	then
		convert -font 'LiberationSans' \
		-pointsize 30 -fill black -draw 'text 90,190 "'"${FIDELIG1_SEND_MSG}"'"' \
		"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.${BGEXTENSION}"
	fi
fi

# Back of the cards generation.

# Blank template for back
cp "${MY_PATH}/images/common/fond_blank.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg"

# Add borders
convert -bordercolor black -border 1 \
"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg"

# Add company infos
if [[ $COMPANY_NAME != "" ]]
then
	convert -font 'LiberationSans' -weight 900 \
	-pointsize 30 -fill black -draw 'text 40,100 "'"${COMPANY_NAME}"'"' \
	"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg"
fi
if [[ $COMPANY_ADDRESS_1 != "" ]]
then
	convert -font 'LiberationSans' \
	-pointsize 30 -fill black -draw 'text 40,150 "'"${COMPANY_ADDRESS_1}"'"' \
	"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg"
fi
if [[ $COMPANY_ADDRESS_2 != "" ]]
then
	convert -font 'LiberationSans' \
	-pointsize 30 -fill black -draw 'text 40,180 "'"${COMPANY_ADDRESS_2}"'"' \
	"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg"
fi
if [[ $COMPANY_PHONE != "" ]]
then
	convert -font 'LiberationSans' \
	-pointsize 30 -fill black -draw 'text 40,230 "'"${FIDELIG1_TEL_MSG}"' '"${COMPANY_PHONE}"'"' \
	"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg"
fi
if [[ $COMPANY_EMAIL != "" ]]
then
	convert -font 'LiberationSans' \
	-pointsize 30 -fill black -draw 'text 40,260 "'"${FIDELIG1_EMAIL_MSG}"' '"${COMPANY_EMAIL}"'"' \
	"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg"
fi

# Add Apple AppStore and Google Play QRCodes
convert -font 'LiberationSans' \
-pointsize 24 -fill black -draw 'text 30,420 "'"$FIDELIG1_USE_YOUR_G1_MSG"'"' \
-pointsize 24 -fill black -draw 'text 30,450 "'"$FIDELIG1_DL_CESIUM_MSG"'"' \
-pointsize 24 -fill black -draw 'text 30,510 "'"$FIDELIG1_CONNECT_MSG"'"' \
"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg"

if [ "$SECURITY" = "ids" ]
then
	convert -font 'LiberationSans' \
	-pointsize 24 -fill black -draw 'text 30,540 "'"$FIDELIG1_TYPE_IDS1_MSG"'"' \
	-pointsize 24 -fill black -draw 'text 30,570 "'"$FIDELIG1_TYPE_IDS2_MSG"'"' \
	"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg"
else
	convert -font 'LiberationSans' \
	-pointsize 24 -fill black -draw 'text 30,540 "'"$FIDELIG1_SCAN_QR1_MSG"'"' \
	-pointsize 24 -fill black -draw 'text 30,570 "'"$FIDELIG1_SCAN_QR2_MSG"'"' \
	-pointsize 24 -fill black -draw 'text 30,600 "'"$FIDELIG1_SCAN_QR3_MSG"'"' \
	"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg"
fi

composite -compose Over -gravity NorthEast -geometry +135+20 -colorspace sRGB "${MY_PATH}/images/common/logo-cesium.png" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg"

composite -compose Over -gravity SouthEast -geometry +60+260 -colorspace sRGB "${MY_PATH}/images/common/android.png" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg"
composite -compose Over -gravity SouthEast -geometry +30+10 -colorspace sRGB "/tmp/fidelig1/GooglePlay_Cesium.QR.png" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg"

composite -compose Over -gravity SouthEast -geometry +330+250 -colorspace sRGB "${MY_PATH}/images/common/apple.png" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg"
composite -compose Over -gravity SouthEast -geometry +260+10 -colorspace sRGB "/tmp/fidelig1/AppStore_Cesium.QR.png" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.BACK.jpg"

# Instruction cards
if [ "$PRINT_INSTRUCTIONS" = "y" ] || [ "$PRINT_SECURITY" = "y" ]
then
	cp "${MY_PATH}/images/common/fond_blank.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg"

	USEFUL_LINKS_POSITION=350
	
	# Add borders
	convert -bordercolor black -border 1 \
	"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg"

	composite -compose Over -gravity NorthWest -geometry +20+50 -colorspace sRGB "${MY_PATH}/images/${LANG}/G1CardsSmall.png" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg"

	convert -font 'LiberationSans' \
	-pointsize 20 -fill black -draw 'text 450,20 "'"$PUBKEY"'"' \
	"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg"

	if [[ "$PRINT_SECURITY" = "y" ]]
	then
		USEFUL_LINKS_POSITION=393

		if [ "$SECURITY" = "qrids" ] || [ "$SECURITY" = "ids" ] || [ "$SECURITY" = "qridspassword" ]
		then
			convert -font 'LiberationSans' -weight 900 \
			-pointsize 30 -fill black -draw 'text 40,263 "'"$FIDELIG1_SECURITY_CONNECT_MSG"'"' \
			-pointsize 30 -fill black -draw 'text 40,313 "'"$FIDELIG1_IDENTIFIER_MSG"'"' \
			-pointsize 30 -fill black -draw 'text 40,348 "'"$FIDELIG1_SECRET_CODE_MSG"'"' \
			"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg"

			convert -font 'LiberationSans' \
			-pointsize 30 -fill black -draw 'text 200,313 "'"${NUMERO}"'"' \
			-pointsize 30 -fill black -draw 'text 230,348 "'"${SECRET}"'"' \
			"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg"
		fi

		if [ "$SECURITY" = "qrids" ] || [ "$SECURITY" = "qridspassword" ]
		then
			convert -font 'LiberationSans' \
			-pointsize 30 -fill black -draw 'text 40,393 "'"${FIDELIG1_SECURITY_CONNECT_QR_OR_MSG}"'"' \
			-pointsize 30 -fill black -draw 'text 40,428 "'"${FIDELIG1_SECURITY_CONNECT_QR_SECRET_MSG}"' '"${UNIQKEY}"'"' \
			"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg"

			USEFUL_LINKS_POSITION=483
		fi

		if [[ "$SECURITY" = "qrpassword" ]]
		then
			convert -font 'LiberationSans' \
			-pointsize 30 -fill black -draw 'text 40,263 "'"$FIDELIG1_SECURITY_CONNECT_QR_MSG"'"' \
			-pointsize 30 -fill black -draw 'text 40,298 "'"${FIDELIG1_SECURITY_CONNECT_QR_SECRET_MSG}"' '"${UNIQKEY}"'"' \
			"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg"

			USEFUL_LINKS_POSITION=423
		fi

	fi

	if [[ "$PRINT_INSTRUCTIONS" = "y" ]]
	then
		convert -font 'LiberationSans' -weight 900 \
		-pointsize 30 -fill black -draw 'text 40,"'"${USEFUL_LINKS_POSITION}"'" "'"${FIDELIG1_USEFUL_LINKS_MSG}"'"' \
		"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg"

		((USEFUL_LINKS_POSITION+=40))
		
		convert -font 'LiberationSans' \
		-pointsize 30 -fill black -draw 'text 40,"'"${USEFUL_LINKS_POSITION}"'" "'"${FIDELIG1_LINK_WHATS_G1_MSG}"' https://www.monnaie-libre.fr' \
		"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg"

		((USEFUL_LINKS_POSITION+=32))

		convert -font 'LiberationSans' \
		-pointsize 30 -fill black -draw 'text 40,"'"${USEFUL_LINKS_POSITION}"'" "'"${FIDELIG1_LINK_FORUM_MSG}"' https://forum.monnaie-libre.fr' \
		"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg"

		((USEFUL_LINKS_POSITION+=32))

		convert -font 'LiberationSans' \
		-pointsize 30 -fill black -draw 'text 40,"'"${USEFUL_LINKS_POSITION}"'" "'"${FIDELIG1_LINK_GCHANGE_MSG}"' https://www.gchange.fr' \
		"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg"
		((USEFUL_LINKS_POSITION+=32))

		convert -font 'LiberationSans' \
		-pointsize 30 -fill black -draw 'text 40,"'"${USEFUL_LINKS_POSITION}"'" "'"${FIDELIG1_LINK_CESIUM_MSG}"' https://www.cesium.app' \
		"/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg" "/tmp/fidelig1/${UNIQID}/${CARDNAME}.CARD.INSTRUCTIONS.jpg"
	fi

fi