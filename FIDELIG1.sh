#!/bin/bash
################################################################################
# Author: Fred (support@qo-op.com)
# Version: 0.1
# License: AGPL-3.0 (https://choosealicense.com/licenses/agpl-3.0/)
################################################################################
MY_PATH="`dirname \"$0\"`"              # relative
MY_PATH="`( cd \"$MY_PATH\" && pwd )`"  # absolutized and normalized
ME="${0##*/}"
################################################################################
# Create and print FideliJune cards
# ${MY_PATH}/FIDELIG1.sh 986397643 nantes
################################################################################
UNIQID="$1"
STYLE="$2"
NAME="$3"
COMPANY_NAME="$4"
COMPANY_ADDRESS_1="$5"
COMPANY_ADDRESS_2="$6"
COMPANY_PHONE="$7"
COMPANY_EMAIL="$8"
SECURITY="$9"
PASSWORD="${10}"
PRINT_INSTRUCTIONS="${11}"
PRINT_SECURITY="${12}"
LANG="${13}"

## PLANCHE DE CARTES
NBbillets=8

[[ $UNIQID == "" ]] && UNIQID=$(date -u +%s%N | cut -b1-13)$RANDOM

if [[ "$NAME" == "" ]]
then
	NAME="FideliĞ1"
fi

BGEXTENSION="jpg"
if [[ ! -f ${MY_PATH}/images/fond${STYLE}.${BGEXTENSION} ]]
then
	BGEXTENSION="png"
fi

export FIDELIG1_FIDELITY_CARDS_MSG
export FIDELIG1_SEND_MSG
export FIDELIG1_RECEIVE_MSG

export FIDELIG1_IDENTIFIER_MSG
export FIDELIG1_SECRET_CODE_MSG
export FIDELIG1_SECRET_MSG

export FIDELIG1_USE_YOUR_G1_MSG
export FIDELIG1_DL_CESIUM_MSG
export FIDELIG1_CONNECT_MSG

export FIDELIG1_TYPE_IDS1_MSG
export FIDELIG1_TYPE_IDS2_MSG
export FIDELIG1_SCAN_QR1_MSG
export FIDELIG1_SCAN_QR2_MSG
export FIDELIG1_SCAN_QR3_MSG

export FIDELIG1_TEL_MSG
export FIDELIG1_EMAIL_MSG

export FIDELIG1_SECURITY_CONNECT_MSG
export FIDELIG1_SECURITY_CONNECT_QR_OR_MSG
export FIDELIG1_SECURITY_CONNECT_QR_MSG
export FIDELIG1_SECURITY_CONNECT_QR_SECRET_MSG
export FIDELIG1_USEFUL_LINKS_MSG
export FIDELIG1_LINK_WHATS_G1_MSG
export FIDELIG1_LINK_FORUM_MSG
export FIDELIG1_LINK_GCHANGE_MSG
export FIDELIG1_LINK_CESIUM_MSG


# COMMON ELEMENTS

# Apple AppStore/Google Play QRCodes for Cesium
qrencode -s 6 -o "/tmp/fidelig1/AppStore_Cesium.QR.png" "https://apps.apple.com/fr/app/cesium-%C4%9F1/id1471028018"
qrencode -s 6 -o "/tmp/fidelig1/GooglePlay_Cesium.QR.png" "https://play.google.com/store/apps/details?id=fr.duniter.cesium"

# CREATION DE $NBbillets BILLETS DE $MONTANT DU
boucle=0;
while [ $boucle -lt $NBbillets ]
do
	boucle=$((boucle+1))
	NUMBER=$(${MY_PATH}/diceware.sh 4 | xargs)
	SECRET=$(${MY_PATH}/diceware.sh 4 | xargs)

	# CREATION CLEF BILLET
	PUBKEY=$(python3 ${MY_PATH}/get_pubkey.py "$NUMBER" "$SECRET")

	UNIQKEY=""
	PRIVKEY=""
	if [ "$SECURITY" = "qrpassword" ] || [ "$SECURITY" = "qridspassword" ]
	then
		if [[ "$PRINT_SECURITY" = 'y' ]]
		then
			UNIQKEY=$(${MY_PATH}/diceware.sh 2 @ | xargs)
		else
			UNIQKEY=$(${MY_PATH}/diceware.sh 1 | xargs)
		fi
		PRIVKEY=$(python3 ${MY_PATH}/get_privkey_password.py "$NUMBER" "$SECRET" "$UNIQKEY")
	else
		if [ "$SECURITY" = "qr" ] || [ "$SECURITY" = "qrids" ]
		then
			PRIVKEY=$(python3 ${MY_PATH}/get_privkey.py "$NUMBER" "$SECRET")
		fi
	fi

	rm -f /tmp/*.dunikey
	
    mkdir -p "/tmp/fidelig1/${UNIQID}"
	# CREATION FICHIER IMAGE BILLET
	echo $(${MY_PATH}/MAKE_FIDELIG1.sh "${NUMBER}" "${SECRET}" "${PUBKEY}" "${PRIVKEY}" "${UNIQKEY}" "${UNIQID}" "${STYLE}" "${NAME}" "${COMPANY_NAME}" "${COMPANY_ADDRESS_1}" "${COMPANY_ADDRESS_2}" "${COMPANY_PHONE}" "${COMPANY_EMAIL}" "${SECURITY}" "${PRINT_INSTRUCTIONS}" "${PRINT_SECURITY}" "${LANG}")
	
done

# MONTAGE DES IMAGES DES BILLETS VERS /tmp/fidelig1/${UNIQID}.pdf
montage /tmp/fidelig1/${UNIQID}/*.CARD.${BGEXTENSION} -tile 2x4 -geometry +80+80 /tmp/fidelig1/${UNIQID}.front.pdf
montage /tmp/fidelig1/${UNIQID}/*.CARD.BACK.jpg -tile 2x4 -geometry +80+80 /tmp/fidelig1/${UNIQID}.back.pdf

if ls /tmp/fidelig1/${UNIQID}/*.CARD.INSTRUCTIONS.jpg 1> /dev/null 2>&1
then
	montage /tmp/fidelig1/${UNIQID}/*.CARD.INSTRUCTIONS.jpg -tile 2x4 -geometry +80+80 /tmp/fidelig1/${UNIQID}.instructions.pdf
fi
# NB!! if "not autorized" then edit /etc/ImageMagick-6/policy.xml and comment
# <!-- <policy domain="coder" rights="none" pattern="PDF" /> -->
# CLEANING TEMP FILES
rm -Rf /tmp/fidelig1/${UNIQID}

mkdir -p ${MY_PATH}/generated/${UNIQID}

# ALLOWS ANY USER TO DELETE
chmod 777 /tmp/fidelig1/${UNIQID}.front.pdf
chmod 777 /tmp/fidelig1/${UNIQID}.back.pdf

if [[ -f /tmp/fidelig1/${UNIQID}.instructions.pdf ]]
then
	chmod 777 /tmp/fidelig1/${UNIQID}.instructions.pdf
	pdfunite /tmp/fidelig1/${UNIQID}.front.pdf /tmp/fidelig1/${UNIQID}.back.pdf /tmp/fidelig1/${UNIQID}.frontback.pdf
	pdfunite /tmp/fidelig1/${UNIQID}.frontback.pdf /tmp/fidelig1/${UNIQID}.instructions.pdf ${MY_PATH}/generated/${UNIQID}/${UNIQID}.cards.pdf

	chmod 777 /tmp/fidelig1/${UNIQID}.frontback.pdf
else
	pdfunite /tmp/fidelig1/${UNIQID}.front.pdf /tmp/fidelig1/${UNIQID}.back.pdf ${MY_PATH}/generated/${UNIQID}/${UNIQID}.cards.pdf
fi

exit
