jQuery(document).ready(function() {
    jQuery('input[name="security"]').bind('change', function() {
        var value = $(this).val();
        var separate_card = jQuery('input[name="security_instructions"]:checked').val();

        if ((value === 'qrpassword' || value === 'qridspassword') && separate_card === 'n') {
            $('div.password input#password').val('');
            $('div.password input#password').removeAttr('disabled');
            $('div.password').show();
        }
        else {
            $('div.password input#password').val('');
            $('div.password input#password').attr('disabled', 'disabled');
            $('div.password').hide();
        }
    });

    jQuery('input[name="security_instructions"]').bind('change', function() {
        var separate_card = $(this).val();
        var security_type = jQuery('input[name="security"]:checked').val();
        
        if ((security_type === 'qrpassword' || security_type === 'qridspassword') && separate_card === 'n') {
            $('div.password input#password').val('');
            $('div.password input#password').removeAttr('disabled');
            $('div.password').show();
        }
        else {
            $('div.password input#password').val('');
            $('div.password input#password').attr('disabled', 'disabled');
            $('div.password').hide();
        }
    });

    jQuery('input[name="style_select"]').bind('change', function() {
        var selectedVal = $(this).val();
        $('input[name="style"]').val(selectedVal);

        if (selectedVal != "") {
            $('img#bgimg').attr('src', 'images/fond' + selectedVal + '.jpg');
            $('img#logo1img').attr('src', 'images/g1' + selectedVal + '.png');
            $('img#logo2img').attr('src', 'images/logo' + selectedVal + '.png');

            $('input[type="file"]').attr('disabled', 'disabled');
        }
        else {
            $('img#bgimg').attr('src', 'images/fond.jpg');
            $('img#logo1img').attr('src', 'images/g1.png');
            $('img#logo2img').attr('src', 'images/logo.png');

            $('input[type="file"]').removeAttr('disabled');
        }
    });

    jQuery('select[name="language"]').bind('change', function() {
        window.location.href = window.location.origin + window.location.pathname + "?lang=" + $(this).val();
    });

    jQuery('input[name="security"]').trigger('change');
    jQuery('input[name="security_instructions"]').trigger('change');
})